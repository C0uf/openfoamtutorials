/*--------------------------------*- C++ -*----------------------------------*\
| =========                 |                                                 |
| \\      /  F ield         | OpenFOAM: The Open Source CFD Toolbox           |
|  \\    /   O peration     | Version:  plus                                  |
|   \\  /    A nd           | Web:      www.OpenFOAM.com                      |
|    \\/     M anipulation  |                                                 |
\*---------------------------------------------------------------------------*/
FoamFile
{
    version     2.0;
    format      ascii;
    class       dictionary;
    object      fvSolution;
}
// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

solvers
{
    cellDisplacement
    {
        solver          PCG;
        preconditioner  DIC;

        tolerance       1e-06;
        relTol          0;
        maxIter         100;
    }

    p
    {
        solver          PBiCGStab;
        preconditioner  DILU;
        tolerance       1e-5;
        relTol          0.01;
        maxIter         10;
    }

    pFinal
    {
        solver          PBiCGStab;
        preconditioner  DILU;
        tolerance       1e-7;
        relTol          0.01;
    }

    pcorr
    {
        $pFinal;
        solver          PCG;
        preconditioner  DIC;
        tolerance 1e-7;
        relTol 0.02;
    }

    pcorrFinal
    {
        $pcorr;
        relTol          0.1;
        tolerance 1e-7;
    }

    "(U|k|epsilon)"
    {
        solver          smoothSolver;
        smoother        symGaussSeidel;
        tolerance       1e-6;
        relTol          0.01;
    }

    "(U|k|epsilon)Final"
    {
        $U;
        tolerance       1e-6;
        relTol          0;
    }
}

PIMPLE
{
    momentumPredictor   no;
    correctPhi          true;
    nOuterCorrectors    10;
    nCorrectors         1;
    nNonOrthogonalCorrectors 0;
    massFluxInterpolation true;
    ddtCorr             false;
    turbOnFinalIterOnly false;
    moveMeshOuterCorrectors yes;
}

relaxationFactors
{
    fields
    {
        p               1;
    }
    equations
    {
        "(U|k|omega)"   1;
        ".*Final"        1.0;
    }
}


// ************************************************************************* //
