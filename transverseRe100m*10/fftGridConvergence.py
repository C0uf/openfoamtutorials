import numpy as np
import matplotlib.pyplot as plt
import scipy.fftpack as fftpack
import os

N = 2500
Nev = 1000
U = 0.0656
D = 0.0016
H = 0.12

def get_t_forceX_forceZ_(folderName):
    forceFile = os.path.join(folderName,"postProcessing/force.dat")
    zfile = os.path.join(folderName,"zcenter")
    xfile = os.path.join(folderName,"zcenter")
    tfile = os.path.join(folderName,"time")
    forces = np.genfromtxt(forceFile,comments="#")
    zc = np.genfromtxt(zfile,comments="#")
    xc = np.genfromtxt(xfile,comments="#")
    tc = np.genfromtxt(tfile,comments="#")
    times = forces[:,0]
    forceX = forces[:,1]
    forceY = forces[:,2]
    forceZ = forces[:,3]
    t = np.linspace(times[len(times)/2],times[-1],N)
    forceZint = np.interp(t, times, forceZ)
    forceXint = np.interp(t, times, forceX)
    zint = np.interp(t, tc, zc)
    xint = np.interp(t, tc, xc)
    
    
    return t,forceXint,forceZint,xint,zint
    

def main():
    

    
    folderList = ["morphing/U*5.5" , "morphing/U*5.5Half" , "overset/U*5.5innerBlockMeshRefine" , "overset/U*5.5innerBlockMeshRefineHalf"]
    
    fcList = ["U*5.5 m f","U*5.5 m c", "U*5.5 o f", "U*5.5 o f",]
    
    colorList = ["b", "g", "r", "k"]
    
    figCl = plt.figure()
    axCl = figCl.add_subplot(1, 1, 1)
        
    figCd = plt.figure()
    axCd = figCd.add_subplot(1, 1, 1)
        
    figFr = plt.figure()
    axFr = figFr.add_subplot(1, 1, 1)
    
    figZ = plt.figure()
    axZ = figZ.add_subplot(1, 1, 1)
    
    figX = plt.figure()
    axX = figX.add_subplot(1, 1, 1)
    
    for folderName,fc,colorP in zip(folderList,fcList,colorList):
                
        print "Folder analyzed: ",folderName
        
        t,forceXint,forceZint,xint,zint = get_t_forceX_forceZ_(folderName)
    
        fftFz = fftpack.fft(forceZint[Nev-1:N-1])
        freq = fftpack.fftfreq(forceZint[Nev-1:N-1].size, d=t[1]-t[0]) 
        
        fftz = fftpack.fft(zint[Nev-1:N-1])
        freqz = fftpack.fftfreq(zint[Nev-1:N-1].size, d=t[1]-t[0]) 
                
        axCl.plot(t, forceZint/(0.5*U*U*D*H), color=colorP,label=r'$U* =$ '+str(fc)+r'',linewidth=1)
        axCl.legend()
        axCl.set_ylim(-1,1)
        axCl.set_xlabel("t [s]")
        axCl.set_ylabel("Cl [-]")
        
        axCd.plot(t, forceXint/(0.5*U*U*D*H), color=colorP,label=r'$U* =$ '+str(fc)+r'',linewidth=1)
        axCd.legend()
        axCd.set_ylim(-3.5,6.5)
        #axCd.set_xlim(180,200)
        axCd.set_xlabel("t [s]")
        axCd.set_ylabel("Cd [-]")
        
        axFr.plot(freq[0:freq.size/2], np.abs(fftFz)[0:fftFz.size/2], color=colorP,label=r'$U* =$ '+str(fc)+r'',linewidth=1)
        axFr.legend()
        axFr.set_xlabel("fr [1/s]")
        axFr.set_ylabel("Power")
        
        axZ.plot(t, np.divide(zint,D), color=colorP,label=r'$U* =$ '+str(fc)+r'',linewidth=1)
        axZ.legend()
        #axZ.set_ylim(-3.5,6.5)
        #axCd.set_xlim(180,200)
        axZ.set_xlabel("t [s]")
        axZ.set_ylabel("z/D [-]")
        
        
        
        #np.savetxt("fftLift.txt",np.transpose([freq,np.abs(fftFz)]))
        print "frequency Fz with the maximum power"
        print freq[np.argmax(np.abs(fftFz))]
        
        print "frequency z with the maximum power"
        print freq[np.argmax(np.abs(fftz))]
        
        print "St number Fz with the maximum power"
        print freq[np.argmax(np.abs(fftFz))]*D/U
        
        print "rms drag coefficient"
        Cd = forceXint[Nev:N]/(0.5*U*U*D*H)
        print np.sqrt(np.mean(np.subtract(Cd,np.mean(Cd))**2))
        
        print "rms lift coefficient"
        print np.sqrt(np.mean((forceZint[Nev:N]/(0.5*U*U*D*H))**2))
        
        print "max z displacement"
        print np.max(np.divide(zint,D))
        
        print "rms x displacement"
        print np.sqrt(np.mean(np.subtract(xint,np.mean(xint))**2))/D
  
    figCl.savefig('ClAllU*55.png')
    figCd.savefig('CdAllU*55.png')
    figFr.savefig('FrAllU*55.png')
    figZ.savefig('ZU*55.png')
    
if __name__== "__main__":
  main()
    
