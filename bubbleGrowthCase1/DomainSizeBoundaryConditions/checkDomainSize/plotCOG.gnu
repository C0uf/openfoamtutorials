set term png
set output 'cOGvarDomaintotWallside.png'
set xlabel 't[s]'
set ylabel 'cOG [m]'
set key left top
set parametric
set yr [0:0.018]

plot '../checkBoundaryConditions/nOuter2nInner2prghPressure/centerOfGravityAir.dat' u 1:2 w l tit 'Lx = 10 mm', 'nOuter2nInner2prghPressureLx15/centerOfGravityAir.dat' u 1:2 w l tit 'Lx = 15 mm' lc 7, 'nOuter2nInner2prghPressureLx20/centerOfGravityAir.dat' u 1:2 w l tit 'Lx = 20 mm', 'nOuter2nInner2prghPressureLx25/centerOfGravityAir.dat' u 1:2 w l tit 'Lx = 25 mm', 0.684,t tit 't detachment experiment' lt 0 
