/*--------------------------------*- C++ -*----------------------------------*\
| =========                 |                                                 |
| \\      /  F ield         | OpenFOAM: The Open Source CFD Toolbox           |
|  \\    /   O peration     | Version:  v1912                                 |
|   \\  /    A nd           | Website:  www.openfoam.com                      |
|    \\/     M anipulation  |                                                 |
\*---------------------------------------------------------------------------*/
FoamFile
{
    version     2.0;
    format      ascii;
    class       dictionary;
    object      controlDict;
}
// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

application     rhoPimpleFoam;

startFrom       latestTime;

startTime       0;

stopAt          endTime;

endTime         0.035;

deltaT          2e-5;

writeControl    adjustable;

writeInterval   0.005;

purgeWrite      0;

writeFormat     ascii;

writePrecision   8;

writeCompression off;

timeFormat      general;

timePrecision   6;

runTimeModifiable true;

functions
{

    FileUpdate1
    {
        type              timeActivatedFileUpdate;
        libs              ("libutilityFunctionObjects.so");
        writeControl      timeStep;
        writeInterval     1;
        fileToUpdate      "<constant>/refineInterval";
        timeVsFile
        (

            (-1   "<constant>/refineInterval50")
            (0.015   "<constant>/refineInterval1e20")
        );
    }
    
    MachNo1
    {
       type            MachNo;
       libs            (fieldFunctionObjects);
       writeControl    adjustable;
       writeInterval   0.005;
    }
    
    forces1
    {
       type          forces;

       libs          ("libforces.so");

       writeControl  timeStep;
       timeInterval  1;

       log           yes;

       patches       ("sphere");
       origin (0 0 0);
       coordinateRotation
        {
            type            axesRotation;
            e3              (0 0 1);
            e1              (1 0 0);
        }

    }
    
    forceCoeffs1
    {
        // Mandatory entries
        type            forceCoeffs;
        libs            ("libforces.so");
        patches         (<list of patch names>);


        // Optional entries

        // Field names
        p               p;
        U               U;
        rho             rho;

        patches       ("sphere");
        // Reference pressure [Pa]
        pRef            1e5;
        
        rhoInf     1.19;

        // Include porosity effects?
        porosity        no;

        // Store and write volume field representations of forces and moments
        writeFields     no;

        // Centre of rotation for moment calculations
        CofR            (0 0 0);

        // Lift direction
        liftDir         (0 0 1);

        // Drag direction
        dragDir         (1 0 0);

        // Pitch axis
        pitchAxis       (0 1 0);

        // Freestream velocity magnitude [m/s]
        magUInf         411;

        // Reference length [m]
        lRef            1;

        // Reference area [m2]
        Aref      2.426e-11;

        
    }
    
    refineFunc
    {
        libs        ("libutilityFunctionObjects.so");

        type coded;
        // Name of on-the-fly generated functionObject
        name refineFunction;
        writeControl  writeTime;
        
        executeControl timeStep;
        
        codeOptions
        #{
            -I$(LIB_SRC)/finiteVolume/lnInclude \
            -I$(LIB_SRC)/OpenFOAM/lnInclude
        #};


        codeInclude
        #{
            #include "volFieldsFwd.H"
            #include "hexMatcher.H"
        #};
        
        codeData
        #{
            
        #};
            
        codeRead
        #{
        
        #};
        
        codeExecute
        #{
           hexMatcher hex;
           static autoPtr<volScalarField> refineField_;
           // check in into database in order to be visible for the 
           // refinement 
           if(!refineField_.valid())
            {
            Info << "Creating refineField_" << nl;

            refineField_.set
            (
                new volScalarField
                (
                    IOobject
                    (
                        "refineField",
                        mesh().time().timeName(),
                        mesh(),
                        IOobject::NO_READ,
                        IOobject::NO_WRITE
                    ),
                mesh(),
                dimensionedScalar("refineField", dimPressure, scalar(0.0))
                )
            );
            }
            volScalarField refineFlag
            (
                IOobject
                (
                    "refineFlag",
                    mesh().time().timeName(),
                    mesh(),
                    IOobject::NO_READ,
                    IOobject::NO_WRITE
                ),
                mesh(),
                dimensionedScalar("zero", dimless, 0.0)
            );
        

            // Ma
            const volScalarField& fieldToUse = mesh().lookupObject<volScalarField>("p");
            
            
            Info << "mesh nCells " << mesh().nCells() << endl;
            Info << " refineField size " << refineField_().size() << endl;
            
            
            
            for (label celli = 0; celli < mesh().nCells(); celli++)
            {
               
               refineFlag[celli] = 0.0;
               
               scalar xc = mesh().C()[celli].component(0);
               scalar yc = mesh().C()[celli].component(1);
               scalar zc = mesh().C()[celli].component(2);
               
               scalar r = sqrt(xc*xc + yc*yc + zc*zc);
               
               //flag out non hex cells and boundary layers which cause errors
               if ( r > 1.3*5.67E-06   )
               {
                   refineFlag[celli] = 1.0;
               }
            }
            
            
            volScalarField cellSize
            (
                IOobject
                (
                    "cellSize",
                    mesh().time().timeName(),
                    mesh(),
                    IOobject::NO_READ,
                    IOobject::NO_WRITE
                ),
                mesh(),
                dimensionedScalar("zero", dimLength, 0.0)
            );
            cellSize.ref() = pow(mesh().V(),1./3.);
            
            refineField_() = mag(fvc::grad(fieldToUse)) * refineFlag * cellSize;
            
            //* Foam::pow(mesh().V(),1./3.);
            
        #};
        
        codeWrite
        #{
 
        #};
    }

}


// ************************************************************************* //
