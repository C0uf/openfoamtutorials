import numpy as np
import matplotlib.pyplot as plt
import scipy.fftpack as fftpack
from scipy import integrate
import os

rho = 1.19
Umag = 411
A = 2.426E-11
pinfty = 100000
D = 5.558E-06

latestTime = 0.035

def get_force(folderName):
    forceFile = os.path.join(folderName,"postProcessing/force.dat")
    forces = np.genfromtxt(forceFile,comments="#")
    forceXtot = forces[:,1]
    forceXp   = forces[:,4]
    forceXvis = forces[:,7]
    
    return forceXtot,forceXp,forceXvis


def print_Lsh_Lrec(folderName,time):
    pTFile = os.path.join(folderName,"postProcessing/Graphs",str(time),"line_T_p.xy")
    pT = np.genfromtxt(pTFile,comments="#")
    UFile = os.path.join(folderName,"postProcessing/Graphs",str(time),"line_U.xy")
    U = np.genfromtxt(UFile,comments="#")
    
    xL = pT[:,0]
    TL = pT[:,1]
    pL   = pT[:,2]
    
    xU = U[:,0]
    Ux = U[:,1]
    
    for index , p in np.ndenumerate(pL):
        if p > 1.1*pinfty:
            print "L stand off = ", abs(xL[index]/D + 1.)/2
            break
        
    for index , U in np.ndenumerate(Ux):
        if (xU[index] > 0 and Ux[index]>=0 and Ux[index[0]-1]<0):
            print "L recirculation = ", abs(xU[index]/D - 1.)/2
            break
    

def main():
    

    
    folderList = ["baseMesh" , "baseMeshFine" , "refineTestsDeltaPStartFromBase" , "refineTestsDeltaPStartFromBase2"]
    
    
    for folderName in folderList:
                
        print "Folder analyzed: ",folderName
        
        fxtot, fxp, fxvis = get_force(folderName)
        
        print "Cd tot = ", 2*fxtot[-1]/(Umag**2*rho*A)
        print "Cd p = ", 2*fxp[-1]/(Umag**2*rho*A)
        print "Cd vis = ", 2*fxvis[-1]/(Umag**2*rho*A)
        
        print_Lsh_Lrec(folderName,latestTime)
        
        
    
if __name__== "__main__":
  main()
    
